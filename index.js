// 1
for (let i = 0; i < 5; i++) {
	setTimeout(function() {
		console.log(i);
	}, i * 1000);
}

// 2
const arr = Array.from({
	length:1000
}, () => Math.round(Math.random() * 900 + 1));
	
// const arr = [1, 4, 5, 1, 3, 10, 19, 13, 9, 8, 7, 4, 9];

let final = arr.filter((item, index) => {
	return arr.indexOf(item) === index;
});

console.log(final); // Array of unique values
console.log(final.length); // How many unique values are generated

// 3
const arr2 = [1, 0, 10, '0', '1', '', NaN, false, null, undefined, false, true].filter(item => item);

console.log(arr2);